repositories:
  - name: evryfs-oss
    url: https://evryfs.github.io/helm-charts/
releases:
  - name: dependency-track
    namespace: software-inventory
    chart: evryfs-oss/dependency-track
    version: v1.5.5
    installed: true
    values:
      -
        ingress:
          enabled: true
          tls:
            enabled: true
            secretName: {{ .Values.cluster.tls_wildcard_secret }}
          annotations:
            kubernetes.io/ingress.class: nginx
          host: depedency-track.{{ .Values.cluster.domain }}
        frontend:
          image:
            tag: 4.9.1
        apiserver:
          image:
            tag: 4.9.1
  - name: dependency-track-cfg
    namespace: software-inventory
    chart: ./chart
    installed: true
    values:
      - cluster_domain: "{{ .Values.cluster.domain }}"
        cluster_tls_wildcard_secret: "{{ .Values.cluster.tls_wildcard_secret }}"
        password: "{{ .Values.cluster.password }}"

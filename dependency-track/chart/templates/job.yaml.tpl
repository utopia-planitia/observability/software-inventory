{{ if .Release.IsInstall }}
apiVersion: batch/v1
kind: Job
metadata:
  name: "setup-job"
spec:
  backoffLimit: 0
  template:
    metadata:
      labels:
        job: setup-job
    spec:
      serviceAccountName: setup-job
      restartPolicy: Never
      initContainers:
        - name: await-startup
          image: ghcr.io/utopia-planitia/helm-tests-image:latest
          command:
            - sh
            - -c
            - until timeout 10 curl --fail http://dependency-track-apiserver/; do echo "not yet up"; sleep 5; done
      containers:
        - name: setup
          image: ghcr.io/utopia-planitia/helm-tests-image:latest
          env:
          - name: DEPENDENCY_TRACK_PASSWORD
            valueFrom:
              secretKeyRef:
                name: dependency-track-admin-password
                key: password
          command:
            - sh
            - -c
            - |
              set -euxo pipefail

              echo "trigger login"
              curl -X 'POST' \
                --silent --max-time 5 \
                -v \
                'http://dependency-track-apiserver/api/v1/user/login' \
                -H 'accept: text/plain' \
                -H 'Content-Type: application/x-www-form-urlencoded' \
                -d 'username=admin&password=admin'
              
              echo "set password"
              curl -X 'POST' \
                --silent --fail --max-time 5 \
                -v \
                'http://dependency-track-apiserver/api/v1/user/forceChangePassword' \
                -H 'accept: text/plain' \
                -H 'Content-Type: application/x-www-form-urlencoded' \
                -d "username=admin&password=admin&newPassword=${DEPENDENCY_TRACK_PASSWORD}&confirmPassword=${DEPENDENCY_TRACK_PASSWORD}"

              echo "fetch token"
              export TOKEN=$(curl -X 'POST' \
                --silent --fail --max-time 5 \
                -v \
                'http://dependency-track-apiserver/api/v1/user/login' \
                -H 'accept: text/plain' \
                -H 'Content-Type: application/x-www-form-urlencoded' \
                -d "username=admin&password=${DEPENDENCY_TRACK_PASSWORD}")

              export TEAM_JSON=$(curl 'http://dependency-track-apiserver/api/v1/team' \
                --silent --fail --max-time 5 \
                -X 'PUT' \
                -H "authorization: Bearer ${TOKEN}" \
                -H 'content-type: application/json' \
                --data-raw '{"name":"SBOM-Operator"}')
              
              export TEAM_UUID=$(echo "${TEAM_JSON}" | jq -r ".uuid")
              export TEAM_TOKEN=$(echo "${TEAM_JSON}" | jq -r ".apiKeys[0].key")

              for permission in BOM_UPLOAD PORTFOLIO_MANAGEMENT PROJECT_CREATION_UPLOAD VIEW_PORTFOLIO
              do
                curl "http://dependency-track-apiserver/api/v1/permission/${permission}/team/${TEAM_UUID}" \
                  --silent --fail --max-time 5 \
                  -X 'POST' \
                  -H "authorization: Bearer ${TOKEN}"
              done

              kubectl create secret generic sbom-operator --from-literal=dependency-track-api-key=${TEAM_TOKEN}
{{ end }}

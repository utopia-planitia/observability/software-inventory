apiVersion: v1
kind: Secret
metadata:
  name: dependency-track-admin-password
type: generic
stringData:
  password: "{{ .Values.password }}"

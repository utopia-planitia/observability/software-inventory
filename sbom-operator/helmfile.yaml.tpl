repositories:
  - name: ckotzbauer
    url: https://ckotzbauer.github.io/helm-charts
releases:
  - name: sbom-operator
    namespace: software-inventory
    chart: ckotzbauer/sbom-operator
    version: 0.30.0
    values:
      -
        envVars:
        - name: SBOM_TARGETS
          value: dtrack
        - name: SBOM_FORMAT
          value: cyclonedx
        - name: SBOM_DTRACK_BASE_URL
          value: http://dependency-track-apiserver
        - name: SBOM_DTRACK_API_KEY
          valueFrom:
            secretKeyRef:
              name: sbom-operator
              key: dependency-track-api-key
